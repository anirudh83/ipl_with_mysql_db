const mysql = require('mysql');
const fs = require('fs'); 
const parse = require('csv-parser');
const { con } = require('./connection.js');

function createDeliveriesTable(){
        return new Promise((resolve, reject) => {
                let sql = `CREATE TABLE IF NOT EXISTS deliveries (
                        match_id int,
                        inning int,
                        batting_team varchar(100),
                        bowling_team varchar(100),
                        overs int,
                        ball int,
                        batsman varchar(100),
                        non_striker varchar(100),
                        bowler varchar(100),
                        is_super_over int,
                        wide_runs int,
                        bye_runs int,
                        legbye_runs int,
                        noball_runs int,
                        penalty_runs int,
                        batsman_runs int,
                        extra_runs int,
                        total_runs int,
                        player_dismissed varchar(100),
                        dismissal_kind varchar(100),
                        fielder varchar(100)
                )`;
                con.connect(function(err) {
                        if (err) {
                                reject(err);
                        }
                        console.log("Connected!");
                        con.query(sql, function (err, result) {
                                if (err){
                                        reject(err);
                                }else{
                                        resolve(result);
                                }           
                        });
                });
        });
}

let getDeliveriesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./data/deliveries.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(Object.values(r));        
        })
        .on('end', () => {
                if(data.length !== 0){
                        resolve(data); 
                }
                else{
                        reject(data);
                }
        });
});
function insertDataIntoDeliveries(){
        return new Promise((resolve, reject)=>{
                getDeliveriesData.then(data=>{
                        let sql = "INSERT INTO deliveries  VALUES ?";              
                        con.query(sql, [data], function(err,result){
                                if (err){
                                        console.log(err);
                                }else{
                                        console.log(result);
                                }
                                
                                con.end();
                        });
                })
                .catch(err=>{
                        console.log(err);
                })
        });
}

async function deliveriesTable(){
        try{
                let isDeliveriesTableCreated = await createDeliveriesTable();
                console.log(isDeliveriesTableCreated);
                await insertDataIntoDeliveries();
        }catch(err){
                console.log(err);
        }

}

deliveriesTable();