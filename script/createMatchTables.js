const mysql = require('mysql');
const fs = require('fs'); 
const parse = require('csv-parser');
const { con } = require('./connection.js');

function createMatchTable(){
        return new Promise((resolve, reject) =>{
                let sql = `CREATE TABLE  IF NOT EXISTS matches (
                        id int,
                        season int,
                        city varchar(255),
                        date date,
                        team1 varchar(255),
                        team2 varchar(255) ,
                        toss_winner varchar(255) ,
                        toss_decision varchar(255),
                        result varchar(255),
                        dl_applied int,
                        winner varchar(255),
                        win_by_runs int,
                        win_by_wickets int,
                        player_of_match varchar(255),
                        venue varchar(255),
                        umpire1 varchar(255),
                        umpire2 varchar(255),
                        umpire3 varchar(255) 
                )`;
                con.connect(function(err) {
                        if (err) {
                                reject(err);
                        }
                        console.log("Connected!");
                        con.query(sql, function (err, result) {
                                if (err){
                                        reject(err);
                                }else{
                                        resolve("table created");
                                }           
                        });
                });
        });
}

let getMatchesData = new Promise((resolve,reject) => {
        let data = [];
        fs.createReadStream('./data/matches.csv')
        .pipe(parse({ delimiter: ',' }))
        .on('data', (r) => {
                data.push(Object.values(r));        
        })
        .on('end', () => {
                if(data.length !== 0){
                        resolve(data); 
                }
                else{
                        reject(data);
                }
        });
});

function insertDataIntoMatchTable(){
        return new Promise((resolve, reject) => {
                getMatchesData.then(data=>{
                        let sql = "INSERT INTO matches  VALUES ?";              
                        con.query(sql, [data], function(err,result){
                                if (err){
                                        console.log(err);
                                }else{
                                        console.log(result);
                                }
                                
                                con.end();
                        });
                })
                .catch(err => {
                        console.log(err);
                })
        });
}

async function matchesTable(){
        try{
                let isTableCreated = await createMatchTable();
                console.log(isTableCreated);
                await insertDataIntoMatchTable();
        }catch(err){
                console.log(err);
        }
}

matchesTable();
